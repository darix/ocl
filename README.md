# ocl

A simple wrapper script for OpenCorsairLink

## Notes

So far only the stuff needed for the example config is implemented.
See also all the TODO in the script.


## config
config file: /etc/ocl.yml

```yaml
devices:
  'H150i Pro':
     pump:
       mode: 3
     fan:
     - channel: 0
       mode: 4
     - channel: 1
       mode: 4
     - channel: 2
       mode: 44
     led:
     - channel: 0
       mode: 5
```

## install

```
$ zypper ar --refresh obs://home:darix:apps home:darix:apps
$ zypper in ruby2.6-rubygem-pry ruby2.6-rubygem-terrapin
```
